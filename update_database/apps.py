from django.apps import AppConfig


class UpdateDatabaseConfig(AppConfig):
    name = 'update_database'
