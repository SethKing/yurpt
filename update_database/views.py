from django.http import HttpResponseRedirect
from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .forms import UploadFileForm
from .updater import updater, clean_up_charts

@login_required
def upload_files(request):
    '''
    View to upload excel file, call file handler and updater if post, then redirect to home page.
    :param request:
    :return: Render upload form for get and redirect to home for post
    '''
    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            upload_file_handler(request.FILES['file'], request.user)
            return HttpResponseRedirect('/')
    else:
        form = UploadFileForm()
    return render(request, 'update_database/upload_files.html', {'form': form})
    
def upload_file_handler(f, uploader):
    with open('tmp/tmp.xlsx', 'wb+') as destination:
        for chunk in f.chunks():
            destination.write(chunk)
    updater('tmp/tmp.xlsx', uploader)
    return

@login_required
def clean_orphans(request):
    '''
    View to call clean up charts to delete orphan charts from directory.
    '''
    clean_up_charts()
    return HttpResponseRedirect('/')