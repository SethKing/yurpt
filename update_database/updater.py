from django.conf import settings
from search_rel_perms.models import Rock_Sample, Flow_Test_Static, Saturation_Dynamic
from search_rel_perms.models import Pore_Vols, Flow_Test_Dynamic, MSCL
from glob import glob
import numpy as np
import openpyxl
import os
from .plotter import perm_v_sat_plots, correction_plots, saturation_profile_plots
make_charts = True

def updater(filename, uploader):
    """
    Reads in formated excel file for single rock sample with multiple flow tests.
    Saves rock sample and connected data to the database.
    :param filename: Name of excel file to process
    :param uploader: Username of uploader - stored with RockSample.
    :return: None
    """
    workbook = openpyxl.load_workbook('tmp/tmp.xlsx', data_only=True, read_only=True)
    rock_sample = get_rock_sample(workbook)
    rs = Rock_Sample(uploader=uploader, **rock_sample)
    # nflow = rock_sample['num_tests']
    rs.save()

    q_sheets = []
    for sheet in workbook.sheetnames:
        if sheet[:2] == 'Q ':
            q_sheets.append([sheet, 'Sat ' + sheet[2:]])

    for q_ws_nm, sat_ws_nm in q_sheets:
        q_ws = workbook[q_ws_nm]
        sat_ws = workbook[sat_ws_nm]
        (saturation_dyns, pore_vol_mods, flow_test_dyn, cal_flow_dyn) = get_flow_test_static(q_ws, sat_ws, rock_sample=rs)
        Saturation_Dynamic.objects.bulk_create(saturation_dyns)
        Pore_Vols.objects.bulk_create(pore_vol_mods)
        Flow_Test_Dynamic.objects.bulk_create(flow_test_dyn)
    if 'MSCL' in workbook.sheetnames:
        mscl_sheet = workbook['MSCL']
        mscl_d = get_mscl(mscl_sheet, rock_sample=rs)
        MSCL.objects.bulk_create(mscl_d)
    return

def get_rock_sample(wb):
    if 'Header' not in wb.sheetnames:
        raise IOError("Can't read this file, Headersheet not found.")
    header_sheet = wb['Header']
    rock_sample = {}
    rock_sample['rock_name'] = header_sheet['B1'].value
    rock_sample['depositional_environment'] = header_sheet['B2'].value
    rock_sample['rock_type'] = header_sheet['B3'].value
    rock_sample['absolute_perm_md'] = header_sheet['B4'].value
    rock_sample['porosity'] = header_sheet['B5'].value
    rock_sample['pore_fluid'] = header_sheet['B6'].value
    rock_sample['displacing_fluid'] = header_sheet['B7'].value
    rock_sample['temperature_c'] = header_sheet['B8'].value
    rock_sample['pore_pressure_mpa'] = header_sheet['B9'].value
    rock_sample['confining_pressure_mpa'] = header_sheet['B10'].value
    rock_sample['pore_volume_ml'] = header_sheet['B11'].value
    rock_sample['length_cm'] = header_sheet['B12'].value
    rock_sample['diameter_cm'] = header_sheet['B13'].value
    rock_sample['notes'] = header_sheet['B14'].value
    rock_sample['image_links'] = ''
    rock_sample['scan_data_loc'] = header_sheet['B15'].value
    
    q_count = 0
    for sheet in wb.sheetnames:
        if sheet[:2] == 'Q ':
            q_count += 1

    rock_sample['num_tests'] = q_count
    
    if 'MSCL' in wb.sheetnames:
        mscl_ws = wb['MSCL']
        rock_sample['mscl_info'] = mscl_ws['A1'].value
        rock_sample['mscl_PWAmp_A'] = int(mscl_ws['B6'].value.split('=')[-1])
        rock_sample['mscl_PWAmp_B'] = int(mscl_ws['C6'].value.split('=')[-1])
        rock_sample['mscl_PWVel_PTO'] = int(mscl_ws['B7'].value.split('=')[-1])
        rock_sample['mscl_Den1_A'] = float(mscl_ws['B8'].value.split('=')[-1])
        rock_sample['mscl_Den1_B'] = float(mscl_ws['C8'].value.split('=')[-1])
        rock_sample['mscl_Den1_C'] = float(mscl_ws['D8'].value.split('=')[-1])
        rock_sample['mscl_MS1_A'] = int(mscl_ws['B9'].value.split('=')[-1])
        rock_sample['mscl_MS1_B'] = int(mscl_ws['C9'].value.split('=')[-1])
        rock_sample['mscl_MS1_Den'] = int(mscl_ws['D9'].value.split('=')[-1])
        rock_sample['mscl_FP_MGD'] = float(mscl_ws['B10'].value.split('=')[-1])
        rock_sample['mscl_FP_WD'] = float(mscl_ws['C10'].value.split('=')[-1])
    else:
        rock_sample['mscl_info'] = ''
        rock_sample['mscl_PWAmp_A'] = 99 
        rock_sample['mscl_PWAmp_B'] = 99
        rock_sample['mscl_PWVel_PTO'] = 99
        rock_sample['mscl_Den1_A'] = 99.9
        rock_sample['mscl_Den1_B'] = 99.9
        rock_sample['mscl_Den1_C'] = 99.9
        rock_sample['mscl_MS1_A'] = 99
        rock_sample['mscl_MS1_B'] = 99
        rock_sample['mscl_MS1_Den'] = 99
        rock_sample['mscl_FP_MGD'] = 99.9
        rock_sample['mscl_FP_WD'] = 99.9
    return rock_sample
    
def get_flow_test_static(q_ws, sat_ws, rock_sample=None):
    
    def count_scans(ws, start_row=14):
        count = 0
        data_raw = ws['A{0}'.format(start_row): 'A{0}'.format(ws.max_row)]
        for val in data_raw:
            try:
                float(val[0].value)
            except (TypeError, ValueError):
                break
            else:
                count += 1
        return count
    
    def get_por_vols(ws):
        row = 13
        start_column = 2
        max_por_vols = 40
        pore_vols = []
        for column in range(start_column, max_por_vols + start_column + 1):
            try:
                v = float(ws.cell(row=row, column=column).value)
            except (TypeError, ValueError):
                break
            else:
                pore_vols.append(v)
        return pore_vols
    
    def read_column(col_num, ws, col_dict):
        col_letter = col_dict[col_num]
        data_raw = ws['{0}14'.format(col_letter): '{0}{1}'.format(col_letter, num_sat_scans+13)]
        data = np.array([raw[0].value for raw in data_raw])
        return data
    
    def read_row(sat_ws, start_row):
        data = np.array([[cell.value for cell in row] 
                         for row in sat_ws.iter_rows(min_row=start_row,
                                                     max_row=start_row,
                                                     min_col=43,
                                                     max_col=num_por_vol_levels+42)])
        if data.dtype != 'float64':
            data[data == '#N/A'] = np.nan
            data[data == '#DIV/0!'] = np.nan
            data[data == '#REF!'] = np.nan
            data = data.astype('float64')
        return data
    
    flow_test_st = {}
    flow_test_st['rock_sample'] = rock_sample
    flow_test_st['q_ml_min'] = sat_ws['AQ1'].value
    
    num_sat_scans = count_scans(sat_ws)
    flow_test_st['num_sat_scans'] = num_sat_scans
    
    por_vol_levels = get_por_vols(sat_ws)
    
    num_por_vol_levels = len(por_vol_levels)
    flow_test_st['num_pore_vol_levels'] = num_por_vol_levels
    
    flow_test_st['sat_alpha'] = sat_ws['AX2'].value
    flow_test_st['sat_beta'] = sat_ws['AX3'].value
    q_a = q_ws['AE4'].value
    col = 'AE'
    if not q_a:
        col = 'AD'
    flow_test_st['q_a'] = q_ws[f'{col}4'].value
    flow_test_st['q_b'] = q_ws[f'{col}5'].value
    flow_test_st['q_a1'] = q_ws[f'{col}6'].value
    flow_test_st['q_b1'] = q_ws[f'{col}7'].value
    # TODO
    #flow_test_st['brooks_corey_params'] = None
    #flow_test_st['van_genuchten_params'] = None
    flow_test = Flow_Test_Static(**flow_test_st)
    flow_test.save()

    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    col_dict = {}
    i = 1
    for letter in alphabet:
        col_dict[i] = letter
        i += 1
    for fletter in alphabet:
        for sletter in alphabet:
            col_dict[i] = fletter + sletter
            i += 1
    
    # Saturation Dynamic
    sat_dyn = {}
    sat_dyn['distance'] = read_column(1, sat_ws, col_dict)

    sat_profs = []
    for i in range(num_por_vol_levels):
        sat_dyn[f'scan_{i+1}'] = read_column(i+2, sat_ws, col_dict)
        sat_profs.append(sat_dyn[f'scan_{i+1}'])
    
    for i in range(num_por_vol_levels):
        sat_dyn[f'pore_vol_{i+1}'] = read_column(i+43, sat_ws, col_dict)


    saturation_dyns = []
    for isat in range(num_sat_scans):
        sat = {}
        sat['distance'] = sat_dyn['distance'][isat]
        for i in range(num_por_vol_levels):
            sat[f'scan_{i+1:02}'] = sat_dyn[f'scan_{i+1}'][isat]
            sat[f'pore_vol_{i+1:02}'] = sat_dyn[f'pore_vol_{i+1}'][isat]
        sd = Saturation_Dynamic(test_id=flow_test, **sat)
        saturation_dyns.append(sd)
    
    # Pore_vols
    pore_vols = {}
    for row in range(num_sat_scans + 14, num_sat_scans+100):
        val = sat_ws.cell(column=44, row=row).value
        if val:
            start_row = row
            break
    pore_vols['vk_ml'] = read_row(sat_ws, start_row)
    
    pore_vols['avg_co2_sat'] = read_row(sat_ws, start_row+2)
    pore_vols['residual_brine_ml'] = read_row(sat_ws, start_row+3)
    
    pore_vols['vi_ml'] = read_row(sat_ws, start_row+5)
    pore_vols['pore_vol_co2_inj'] = read_row(sat_ws, start_row+6)
    pore_vols['vi_per_vk'] = read_row(sat_ws, start_row+7)
    
    pore_vols['corrected_vk_ml'] = read_row(sat_ws, start_row+9)
    
    pore_vols['corrected_avg_co2_sat'] = read_row(sat_ws, start_row+11)
    pore_vols['corrected_residual_brine_ml'] = read_row(sat_ws, start_row+12)
    pore_vols['corrected_vi_per_vk'] = read_row(sat_ws, start_row+16)

    if make_charts:
        folder = os.path.join(settings.MEDIA_ROOT, 'saturation_charts')
        basename = f'{flow_test.id:06}_saturations.png'
        filename = os.path.join(folder, basename)
        title = f'{flow_test.rock_sample.rock_name} {flow_test.q_ml_min} ml/min Saturation Profiles'
        saturation_profile_plots(sat_dyn['distance'], sat_profs, por_vol_levels,
                                 save_fig=filename, title=title)
        flow_test.saturation_profile_chart = basename

        basename = f'{flow_test.id:06}_correction.png'
        filename = os.path.join(settings.MEDIA_ROOT, 'correction_charts', basename)
        title = f'{flow_test.rock_sample.rock_name} {flow_test.q_ml_min} ml/min Volume of $CO_2$ in Core Correction'
        correction_plots(pore_vols['pore_vol_co2_inj'][0, :],
                         pore_vols['vk_ml'][0, :],
                         pore_vols['corrected_vk_ml'][0, :],
                         title=title,
                         save_fig=filename)
        flow_test.correction_chart = basename
        flow_test.save()

    pore_vol_mods = []
    for i, pv in enumerate(por_vol_levels):
        pvd = {'pore_volume_ml': pv}
        pvd['vk_ml'] = pore_vols['vk_ml'][0, i]
    
        pvd['avg_co2_sat'] = pore_vols['avg_co2_sat'][0, i]
        pvd['residual_brine_ml'] = pore_vols['residual_brine_ml'][0, i]
        
        pvd['vi_ml'] = pore_vols['vi_ml'][0, i]
        pvd['pore_vol_co2_inj'] = pore_vols['pore_vol_co2_inj'][0, i]
        pvd['vi_per_vk'] = pore_vols['vi_per_vk'][0, i]
        
        pvd['corrected_vk_ml'] = pore_vols['corrected_vk_ml'][0, i]
        
        pvd['corrected_avg_co2_sat'] = pore_vols['corrected_avg_co2_sat'][0, i]
        pvd['corrected_residual_brine_ml'] = pore_vols['corrected_residual_brine_ml'][0, i]
        pvd['corrected_vi_per_vk'] = pore_vols['corrected_vi_per_vk'][0, i]
        pvm = Pore_Vols(test_id=flow_test, **pvd)
        pore_vol_mods.append(pvm)
    
    (flow_test_dyn, cal_flow_dyn) = get_flow_test_dynamic(q_ws, test_id=flow_test)
    
    return (saturation_dyns, pore_vol_mods, flow_test_dyn, cal_flow_dyn)
    
def get_flow_test_dynamic(ws, test_id=None):
    def count_q_scans(ws):
        count = 0
        data_raw = ws['A12': 'A{0}'.format(ws.max_row)]
        for val in data_raw:
            if not val[0].value:
                break
            else:
                count += 1
        return count

    def find_calculated_data_start(ws):
        count = 15
        data_raw = ws['V15': 'V{0}'.format(num_points+11)]
        for val in data_raw:
            if type(val[0].value) == float:
                break
            else:
                count += 1
        return count

    def read_column(col_name, ws, headers):
        try:
            col_num = headers.index(col_name) + 1
        except ValueError:
            return [None]*num_points
        col_letter = col_dict[col_num]
        data_raw = ws['{0}12'.format(col_letter): '{0}{1}'.format(col_letter, num_points+11)]
        data = np.array([raw[0].value for raw in data_raw])
        return data

    def read_cal_column(col_letter, cal_start, ws):
        data_raw = ws['{0}{1}'.format(col_letter, cal_start): '{0}{1}'.format(col_letter, num_points+11)]
        data = np.array([raw[0].value for raw in data_raw])
        if data.dtype != 'float64':
            data[data == '#N/A'] = np.nan
            data[data == '#DIV/0!'] = np.nan
            data[data == '#REF!'] = np.nan
            data = np.genfromtxt(data)
        return data
    
    num_points = count_q_scans(ws)
    
    header_row = 11
    headers = []
    column = 1
    while column < 100:
        new_head = ws.cell(row=header_row, column=column).value
        if new_head:
            headers.append(new_head)
            column += 1
        else:
            break
    alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    col_dict = {}
    i = 1
    for letter in alphabet:
        col_dict[i] = letter
        i += 1
    for fletter in alphabet:
        for sletter in alphabet:
            col_dict[i+1] = fletter + sletter
            i += 1
    
    test_id = test_id
    time_adj = read_column('Time (adjusted)', ws, headers)
    time = read_column('Time', ws, headers)
    receiving_a_p_psi = read_column('Receiving A  P (psi)', ws, headers)
    receiving_a_vol_ml = read_column('Receiving A Vol (ml)', ws, headers)
    receiving_a_q_ml_min = read_column('Receiving A Q (ml/min)', ws, headers)
    receiving_b_p_psi = read_column('Receiving B  P (psi)', ws, headers)
    receiving_b_vol_ml = read_column('Receiving B Vol (ml)', ws, headers)
    receiving_b_q_ml_min = read_column('Receiving B Q (ml/min)', ws, headers)
    delivery_a_p_psi = read_column('Delivery A  P (psi)', ws, headers)
    delivery_a_vol_ml = read_column('Delivery A Vol (ml)', ws, headers)
    delivery_a_q_ml_min = read_column('Delivery A Q (ml/min)', ws, headers)
    delivery_b_p_psi = read_column('Delivery B  P (psi)', ws, headers)
    delivery_b_vol_ml = read_column('Delivery B Vol (ml)', ws, headers)
    delivery_b_q_ml_min = read_column('Delivery B Q (ml/min)', ws, headers)
    diff_p_high_psi = read_column('Diff P High (PSI)', ws, headers)
    setra_delivery_p_psi = read_column('Setra Deliver (psi)', ws, headers)
    setra_receive_p_psi = read_column('Setra Receive (psi)', ws, headers)
    diff_p_low_psi = read_column('Diff P Low (PSI)', ws, headers)
    temperature_c = read_column('Temp (C)', ws, headers)
    
    cal_start = find_calculated_data_start(ws)

    delta_p_pa = read_cal_column('W', cal_start, ws)
    v_i = read_cal_column('X', cal_start, ws)
    norm_v_i = read_cal_column('Y', cal_start, ws)
    avg_s_co2 = read_cal_column('Z', cal_start, ws)
    s_co2_2 = read_cal_column('AA', cal_start, ws)
    m_co2_2 = read_cal_column('AB', cal_start, ws)
    f_co2_2 = read_cal_column('AC', cal_start, ws)
    f_w_2 = read_cal_column('AD', cal_start, ws)
    y_sco2_2 = read_cal_column('AE', cal_start, ws)
    k_rco2 = read_cal_column('AF', cal_start, ws)
    k_rw = read_cal_column('AG', cal_start, ws)
    data_raw = ws['{0}{1}'.format('AH', cal_start): '{0}{1}'.format('AH', num_points + 11)]
    bt = np.array([raw[0].value for raw in data_raw])
    for bt_time, bt_val in enumerate(bt):
        if bt_val == 1:
            break
    else:
        bt_time = 0

    if make_charts:
        basename = f'{test_id.id:06}_flow_test.png'
        filename = os.path.join(settings.MEDIA_ROOT, 'flow_test_charts', basename)

        title = f'{test_id.rock_sample.rock_name} {test_id.q_ml_min} ml/min Toth et al. Derived Relative Permeabilities'
        perm_v_sat_plots(s_co2_2, k_rco2, k_rw, save_fig=filename, start_point=bt_time, title=title)
        test_id.flow_test_chart = basename
        test_id.save()

    flow_tests_dyn = []
    cal_flow_dyn = []
    for i in range(num_points):
        ftd = {}
        ftd['time_adj'] = time_adj[i]
        ftd['time'] = time[i]
        ftd['receiving_a_p_psi'] = receiving_a_p_psi[i]
        ftd['receiving_a_vol_ml'] = receiving_a_vol_ml[i]
        ftd['receiving_a_q_ml_min'] = receiving_a_q_ml_min[i]
        ftd['receiving_b_p_psi'] = receiving_b_p_psi[i]
        ftd['receiving_b_vol_ml'] = receiving_b_vol_ml[i]
        ftd['receiving_b_q_ml_min'] = receiving_b_q_ml_min[i]
        ftd['delivery_a_p_psi'] = delivery_a_p_psi[i]
        ftd['delivery_a_vol_ml'] = delivery_a_vol_ml[i]
        ftd['delivery_a_q_ml_min'] = delivery_a_q_ml_min[i]
        ftd['delivery_b_p_psi'] = delivery_b_p_psi[i]
        ftd['delivery_b_vol_ml'] = delivery_b_vol_ml[i]
        ftd['delivery_b_q_ml_min'] = delivery_b_q_ml_min[i]
        ftd['diff_p_high_psi'] = diff_p_high_psi[i]
        ftd['setra_delivery_p_psi'] = setra_delivery_p_psi[i]
        ftd['setra_receive_p_psi'] = setra_receive_p_psi[i]
        ftd['diff_p_low_psi'] = diff_p_low_psi[i]
        ftd['temperature_c'] = temperature_c[i]
        
        cal_i = i - cal_start + 12
        if cal_i >= 0:
            ftd['delta_p_pa'] = delta_p_pa[cal_i]
            ftd['v_i'] = v_i[cal_i]
            ftd['norm_v_i'] = norm_v_i[cal_i]
            ftd['avg_s_co2'] = avg_s_co2[cal_i]
            ftd['s_co2_2'] = s_co2_2[cal_i]
            ftd['m_co2_2'] = m_co2_2[cal_i]
            ftd['f_co2_2'] = f_co2_2[cal_i]
            ftd['f_w_2'] = f_w_2[cal_i]
            ftd['y_sco2_2'] = y_sco2_2[cal_i]
            ftd['k_rco2'] = k_rco2[cal_i]
            ftd['k_rw'] = k_rw[cal_i]
        flow_tests_dyn.append(Flow_Test_Dynamic(test_id=test_id, **ftd))
            
    
    return (flow_tests_dyn, cal_flow_dyn)

def get_mscl(mscl_sheet, rock_sample=None):
    def count_scans(ws):
        count = 0
        data_raw = ws['A16': 'A{0}'.format(ws.max_row)]
        for val in data_raw:
            if not val[0].value:
                break
            else:
                count += 1
        return count
    
    def read_row(ws, start_row, end_row):
        data = np.array([[cell.value for cell in row] 
                            for row in ws.iter_rows(min_row=start_row,
                                                    max_row=end_row,
                                                    min_col=1,
                                                    max_col=69)])
        return data

    var_names = []
    for i in range(69):
        var_names.append(mscl_sheet.cell(row=13, column=i+1).value)
    start_scan = 16
    n_scans = count_scans(mscl_sheet)
    end_scan = n_scans + start_scan 
    data = read_row(mscl_sheet, start_scan, end_scan)
    
    mscl_data_names = [
        'sb_depth_cm',
        'sect_num',
        'sect_depth_cm',
        'pwamp',
        'pwvel_mps',
        'den1_gpcc',
        'ms1_cgs',
        'imp',
        'fp',
        'xrf',
        'xrf_live_time',
        'ti_ppm',
        'ti_error',
        'v_ppm',
        'v_error',
        'cr_ppm',
        'cr_error',
        'mn_ppm',
        'mn_error',
        'fe_ppm',
        'fe_error',
        'co_ppm',
        'co_error',
        'ni_ppm',
        'ni_error',
        'cu_ppm',
        'cu_error',
        'zn_ppm',
        'zn_error',
        'as_ppm',
        'as_error',
        'zr_ppm',
        'zr_error',
        'mo_ppm',
        'mo_error',
        'ag_ppm',
        'ag_error',
        'cd_ppm',
        'cd_error',
        'sn_ppm',
        'sn_error',
        'sb_ppm',
        'sb_error',
        'hf_ppm',
        'hf_error',
        'w_ppm',
        'w_error',
        'pb_ppm',
        'pb_error',
        'bi_ppm',
        'bi_error',
        'le_ppm',
        'le_error',
        'mg_ppm',
        'mg_error',
        'al_ppm',
        'al_error',
        'si_ppm',
        'si_error',
        'p_ppm',
        'p_error',
        's_ppm',
        's_error',
        'cl_ppm',
        'cl_error',
        'k_ppm',
        'k_error',
        'ca_ppm',
        'ca_error']
    
    mscl_d = []
    for i_scan in range(n_scans):
        mscl_data = {}
        for i_var in range(69):
            mscl_data[mscl_data_names[i_var]] = data[i_scan, i_var]
        mscl_d.append(MSCL(rock_sample=rock_sample, **mscl_data))
    return mscl_d

def clean_up_charts():
    """
    Method to iterate through 3 chart type folders and call clean up routine to remove orphan charts.
    Orphan charts being charts from flow tests that have been deleted from the database.
    :return: None
    """
    base_folder = settings.MEDIA_ROOT
    folders = ['flow_test_charts', 'saturation_charts', 'correction_charts']
    for folder in folders:
        fold = os.path.join(base_folder, folder)
        clean_up_folder(fold)
    return

def clean_up_folder(folder):
    """
    Searches input folder for charts (*.png files) with an id number (first 6 digits of chart name)
    that no longer exists in the database (has been deleted).
    :param folder: str folder name to search
    :return: None
    """
    ids = Flow_Test_Static.objects.all().values_list('id', flat=True)
    files = glob(os.path.join(folder, '*.png'))
    for cfile in files:
        cfid = int(os.path.basename(cfile)[:6])
        if cfid not in ids:
            os.remove(cfile)
    return