from django.urls import path
from . import views

urlpatterns = [
	path('', views.upload_files, name='update-home'),
	path('clean/', views.clean_orphans, name='clean-orphans')
]