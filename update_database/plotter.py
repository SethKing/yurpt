import numpy as np
import matplotlib.pyplot as plt


def perm_v_sat_plots(s_co2, k_rco2, k_rw, start_point=0, save_fig='test.png', title=None):
    s_co2 = s_co2[start_point:]
    k_rco2 = k_rco2[start_point:]
    k_rw = k_rw[start_point:]

    fig = plt.figure('perm_v_sat_plot', figsize=(10, 5))
    plt.clf()

    plt.semilogy(s_co2, k_rco2, 'b', label='$k_{rCO2}$')
    plt.semilogy(s_co2, k_rw, 'r', label='$k_{rw}$')

    if start_point != 0:
        plt.axvline(x=s_co2[0], color='k', linestyle='--', label='Breakthrough')

    fig.legend(loc=1)
    plt.xlabel('$S_{CO2}$')
    plt.grid(True, alpha=0.4)
    if title:
        plt.title(title)
    plt.tight_layout()
    fig.savefig(save_fig)

    return

def correction_plots(vivp, vk, corrected_vk, save_fig='test.png', title=None):
    fig = plt.figure(figsize=(10, 5))
    plt.clf()

    plt.plot(vivp, vk, "^-", label='$V_k$')
    plt.plot(vivp, corrected_vk, ".-", label='Corrected $V_k$')

    plt.xlabel('$V_i/V_p$')
    plt.ylabel('$V_k$')
    plt.grid(True, alpha=0.4)
    plt.legend()
    if title:
        plt.title(title)
    plt.tight_layout()
    fig.savefig(save_fig)
    return

def saturation_profile_plots(dist, sats, por_vol_levels, save_fig='test.png', title=None):
    fig = plt.figure(figsize=(10, 5))
    plt.clf()
    ax = plt.subplot(111)

    poss_mark = ['.', 'o', 'v', '^', '>', '<', 's', 'p', '*', 'h', 'H', 'D', 'd',
                 '1', '2', '3', '4', '8', 'P', '+', 'x', 'X']
    poss_mark.extend(poss_mark)

    for i, sat in enumerate(sats):
        plt.plot(dist, sat, marker=poss_mark[i], markersize=3, label=f'{por_vol_levels[i]:0.2f}')

    plt.xlabel('Distance (mm)')
    plt.ylabel('Saturation (%)')
    plt.xlim(left=0)
    plt.ylim(bottom=0)
    plt.grid(True, alpha=0.4)

    box = ax.get_position()
    ax.set_position([box.x0, box.y0, box.width * 0.85, box.height])
    # Put a legend to the right of the current axis
    ax.legend(title='Pore Volumes Injected', loc='center left', bbox_to_anchor=(1, 0.5), ncol=2)
    #plt.legend(title='Pore Volumes Injected')
    if title:
        plt.title(title)
    #plt.tight_layout()
    fig.savefig(save_fig)
    return
