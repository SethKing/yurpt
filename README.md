## To run locally outside of docker:
In anaconda prompt create a new virtual environment for YURPT "conda create -n YURPT python=3.6 pip"
After environment is created activate it "conda activate YURPT"
Pip install the required libraries "pip install -r pip_req.txt"

## To setup YURPT:
 - Create a secret_key.txt file with a secret key.
 - Run python manage.py makemigrations
 - Run python manage.py migrate
 - Run python manage.py createsuperuser and follow prompts to create a user.
 - Run python manage.py runserver to start the server - leave command prompt open
 - In browser go to localhost:8000 to see if server is running properly.
 - Go to localhost:8000/login to login as user created earlier
 - Go to localhost:8000/update to upload excel files into the database (must be logged in)
 - Once data is uploaded front page should update with Rock Samples loaded into database
 - Go to localhost:8000/admin to delete rock sample (in rock_sample table). 
(Rock sample names are unique, so they must be deleted before being reuploaded for any reason)
  
## To run inside a docker environment:
 * Check setup.py file is configured for postgres database 
 (use database setting from setting_postgres.py)
 * run docker-compose up to set up environment.
 * Once environment builds and server is running, kill the server with ctrl-c
 * Using the 'docker-compose run web python manage.py $cmd' command run the following commands for $cmd: 
   * makemigrations
   * migrate
   * createsuperuser (then follow prompts to create user)
 * Run the docker-compose up command to run the server and upload data.