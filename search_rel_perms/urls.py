from django.urls import path
from . import views


urlpatterns = [
	path('', views.home, name='search-home'),
    path('list/', views.list, name='list'),
    path('about/', views.about, name='search-about'),
    path('about/bib/', views.about_bibtex, name='about_bib'),
    path('rock_sample/<int:ids>/', views.rock_sample, name='rock_sample'),
    path('flow_test/<int:ids>/', views.flow_test, name='flow_test'),
    path('flow_test_defs/', views.flow_test_defs, name='flow_test_defs'),
    path('saturation_data/<int:ids>/', views.saturation_data, name='saturation_data'),
	path('mscl/<int:ids>/', views.mscl, name='mscl'),
    path('por_vol/<int:ids>/', views.por_vol_corrections, name='por_vol'),
]