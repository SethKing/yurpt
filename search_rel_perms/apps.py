from django.apps import AppConfig


class SearchRelPermsConfig(AppConfig):
    name = 'search_rel_perms'
