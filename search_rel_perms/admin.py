from django.contrib import admin
from .models import Rock_Sample, Flow_Test_Static, Saturation_Dynamic

admin.site.register(Rock_Sample)
admin.site.register(Flow_Test_Static)
admin.site.register(Saturation_Dynamic)