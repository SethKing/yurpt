import django_tables2 as tables
from django.utils.safestring import mark_safe
from .models import Flow_Test_Dynamic, Saturation_Dynamic, MSCL

class NumberColumn(tables.Column):
    def render(self, value):
        return '{:#.4g}'.format(value)

class Saturation_Dynamic_Table(tables.Table):
    distance = tables.Column(orderable=False, verbose_name='Distance (mm)')
    scan_01 = NumberColumn(orderable=False)
    scan_02 = NumberColumn(orderable=False)
    scan_03 = NumberColumn(orderable=False)
    scan_04 = NumberColumn(orderable=False)
    scan_05 = NumberColumn(orderable=False)
    scan_06 = NumberColumn(orderable=False)
    scan_07 = NumberColumn(orderable=False)
    scan_08 = NumberColumn(orderable=False)
    scan_09 = NumberColumn(orderable=False)
    scan_10 = NumberColumn(orderable=False)
    scan_11 = NumberColumn(orderable=False)
    scan_12 = NumberColumn(orderable=False)
    scan_13 = NumberColumn(orderable=False)
    scan_14 = NumberColumn(orderable=False)
    scan_15 = NumberColumn(orderable=False)
    scan_16 = NumberColumn(orderable=False)
    scan_17 = NumberColumn(orderable=False)
    scan_18 = NumberColumn(orderable=False)
    scan_19 = NumberColumn(orderable=False)
    scan_20 = NumberColumn(orderable=False)
    scan_21 = NumberColumn(orderable=False)
    scan_22 = NumberColumn(orderable=False)
    scan_23 = NumberColumn(orderable=False)
    scan_24 = NumberColumn(orderable=False)
    scan_25 = NumberColumn(orderable=False)
    scan_26 = NumberColumn(orderable=False)
    scan_27 = NumberColumn(orderable=False)
    scan_28 = NumberColumn(orderable=False)
    scan_29 = NumberColumn(orderable=False)
    scan_30 = NumberColumn(orderable=False)
    scan_31 = NumberColumn(orderable=False)
    scan_32 = NumberColumn(orderable=False)
    scan_33 = NumberColumn(orderable=False)
    scan_34 = NumberColumn(orderable=False)
    scan_35 = NumberColumn(orderable=False)
    scan_36 = NumberColumn(orderable=False)
    scan_37 = NumberColumn(orderable=False)
    scan_38 = NumberColumn(orderable=False)
    scan_39 = NumberColumn(orderable=False)
    scan_40 = NumberColumn(orderable=False)
    pore_vol_01 = NumberColumn(orderable=False)
    pore_vol_02 = NumberColumn(orderable=False)
    pore_vol_03 = NumberColumn(orderable=False)
    pore_vol_04 = NumberColumn(orderable=False)
    pore_vol_05 = NumberColumn(orderable=False)
    pore_vol_06 = NumberColumn(orderable=False)
    pore_vol_07 = NumberColumn(orderable=False)
    pore_vol_08 = NumberColumn(orderable=False)
    pore_vol_09 = NumberColumn(orderable=False)
    pore_vol_10 = NumberColumn(orderable=False)
    pore_vol_11 = NumberColumn(orderable=False)
    pore_vol_12 = NumberColumn(orderable=False)
    pore_vol_13 = NumberColumn(orderable=False)
    pore_vol_14 = NumberColumn(orderable=False)
    pore_vol_15 = NumberColumn(orderable=False)
    pore_vol_16 = NumberColumn(orderable=False)
    pore_vol_17 = NumberColumn(orderable=False)
    pore_vol_18 = NumberColumn(orderable=False)
    pore_vol_19 = NumberColumn(orderable=False)
    pore_vol_20 = NumberColumn(orderable=False)
    pore_vol_21 = NumberColumn(orderable=False)
    pore_vol_22 = NumberColumn(orderable=False)
    pore_vol_23 = NumberColumn(orderable=False)
    pore_vol_24 = NumberColumn(orderable=False)
    pore_vol_25 = NumberColumn(orderable=False)
    pore_vol_26 = NumberColumn(orderable=False)
    pore_vol_27 = NumberColumn(orderable=False)
    pore_vol_28 = NumberColumn(orderable=False)
    pore_vol_29 = NumberColumn(orderable=False)
    pore_vol_30 = NumberColumn(orderable=False)
    pore_vol_31 = NumberColumn(orderable=False)
    pore_vol_32 = NumberColumn(orderable=False)
    pore_vol_33 = NumberColumn(orderable=False)
    pore_vol_34 = NumberColumn(orderable=False)
    pore_vol_35 = NumberColumn(orderable=False)
    pore_vol_36 = NumberColumn(orderable=False)
    pore_vol_37 = NumberColumn(orderable=False)
    pore_vol_38 = NumberColumn(orderable=False)
    pore_vol_39 = NumberColumn(orderable=False)
    pore_vol_40 = NumberColumn(orderable=False)

    class Meta:
        model = Saturation_Dynamic
        attrs = {'class': "table table-striped table-sm"}

class MSCL_Table(tables.Table):
    rock_sample_id = tables.Column(orderable=False)
    sb_depth_cm  = tables.Column(orderable=False)
    sect_num  = tables.Column(orderable=False)
    sect_depth_cm  = tables.Column(orderable=False)
    pwamp = tables.Column(orderable=False)
    pwvel_mps  = tables.Column(orderable=False)
    den1_gpcc  = tables.Column(orderable=False)
    ms1_cgs = tables.Column(orderable=False)
    imp = tables.Column(orderable=False)
    fp = tables.Column(orderable=False)
    xrf = tables.Column(orderable=False)
    xrf_live_time = tables.Column(orderable=False)
    ti_ppm = tables.Column(orderable=False)
    ti_error = tables.Column(orderable=False)
    v_ppm = tables.Column(orderable=False)
    v_error = tables.Column(orderable=False)
    cr_ppm = tables.Column(orderable=False)
    cr_error = tables.Column(orderable=False)
    mn_ppm = tables.Column(orderable=False)
    mn_error = tables.Column(orderable=False)
    fe_ppm = tables.Column(orderable=False)
    fe_error = tables.Column(orderable=False)
    co_ppm = tables.Column(orderable=False)
    co_error = tables.Column(orderable=False)
    ni_ppm = tables.Column(orderable=False)
    ni_error = tables.Column(orderable=False)
    cu_ppm = tables.Column(orderable=False)
    cu_error = tables.Column(orderable=False)
    zn_ppm = tables.Column(orderable=False)
    zn_error = tables.Column(orderable=False)
    as_ppm = tables.Column(orderable=False)
    as_error = tables.Column(orderable=False)
    zr_ppm = tables.Column(orderable=False)
    zr_error = tables.Column(orderable=False)
    mo_ppm = tables.Column(orderable=False)
    mo_error = tables.Column(orderable=False)
    ag_ppm = tables.Column(orderable=False)
    ag_error = tables.Column(orderable=False)
    cd_ppm = tables.Column(orderable=False)
    cd_error = tables.Column(orderable=False)
    sn_ppm = tables.Column(orderable=False)
    sn_error = tables.Column(orderable=False)
    sb_ppm = tables.Column(orderable=False)
    sb_error = tables.Column(orderable=False)
    hf_ppm = tables.Column(orderable=False)
    hf_error = tables.Column(orderable=False)
    w_ppm = tables.Column(orderable=False)
    w_error = tables.Column(orderable=False)
    pb_ppm = tables.Column(orderable=False)
    pb_error = tables.Column(orderable=False)
    bi_ppm = tables.Column(orderable=False)
    bi_error = tables.Column(orderable=False)
    le_ppm = tables.Column(orderable=False)
    le_error = tables.Column(orderable=False)
    mg_ppm = tables.Column(orderable=False)
    mg_error = tables.Column(orderable=False)
    al_ppm = tables.Column(orderable=False)
    al_error = tables.Column(orderable=False)
    si_ppm = tables.Column(orderable=False)
    si_error = tables.Column(orderable=False)
    p_ppm = tables.Column(orderable=False)
    p_error = tables.Column(orderable=False)
    s_ppm = tables.Column(orderable=False)
    s_error = tables.Column(orderable=False)
    cl_ppm = tables.Column(orderable=False)
    cl_error = tables.Column(orderable=False)
    k_ppm = tables.Column(orderable=False)
    k_error = tables.Column(orderable=False)
    ca_ppm = tables.Column(orderable=False)
    ca_error = tables.Column(orderable=False)
    
    class Meta:
        model = MSCL
        attrs = {'class': "table table-striped table-sm"}
        
class Flow_Test_Table(tables.Table):
    test_id = tables.Column(orderable=False)
    time_adj = tables.DateTimeColumn(format='g:i:s', orderable=False, verbose_name='Time (adj)')
    time = tables.DateTimeColumn(format='g:i:s', orderable=False)
    receiving_a_p_psi = tables.Column(orderable=False)
    receiving_a_vol_ml = tables.Column(orderable=False)
    receiving_a_q_ml_min = tables.Column(orderable=False)
    receiving_b_p_psi = tables.Column(orderable=False)
    receiving_b_vol_ml = tables.Column(orderable=False)
    receiving_b_q_ml_min = tables.Column(orderable=False)
    delivery_a_p_psi = tables.Column(orderable=False)
    delivery_a_vol_ml = tables.Column(orderable=False)
    delivery_a_q_ml_min = tables.Column(orderable=False)
    delivery_b_p_psi = tables.Column(orderable=False)
    delivery_b_vol_ml = tables.Column(orderable=False)
    delivery_b_q_ml_min = tables.Column(orderable=False)
    diff_p_high_psi = tables.Column(orderable=False)
    setra_delivery_p_psi = tables.Column(orderable=False)
    setra_receive_p_psi = tables.Column(orderable=False)
    diff_p_low_psi = tables.Column(orderable=False)
    temperature_c = tables.Column(orderable=False, verbose_name='Temperature \u2103')
    delta_p_pa = NumberColumn(orderable=False, verbose_name='Delta P (Pa)')
    v_i = NumberColumn(orderable=False, verbose_name=mark_safe('V<sub>i</sub>'))
    norm_v_i = NumberColumn(orderable=False, verbose_name=mark_safe('V<sub>i</sub>/V<sub>p</sub>'))
    avg_s_co2 = NumberColumn(orderable=False, verbose_name=mark_safe('S<sub>CO2,avg</sub>'))
    s_co2_2 = NumberColumn(orderable=False, verbose_name=mark_safe('S<sub>CO2,2</sub>'))
    m_co2_2 = NumberColumn(orderable=False, verbose_name=mark_safe('M<sub>CO2,2</sub>'))
    f_co2_2 = NumberColumn(orderable=False, verbose_name=mark_safe('&fnof;<sub>CO2,2</sub>'))
    f_w_2 = NumberColumn(orderable=False, verbose_name=mark_safe('&fnof;<sub>W,2</sub>'))
    y_sco2_2 = NumberColumn(orderable=False, verbose_name=mark_safe('Y(S<sub>CO2,2</sub>)'))
    k_rco2 = NumberColumn(orderable=False, verbose_name=mark_safe('k<sub>rCO2</sub>'))
    k_rw = NumberColumn(orderable=False, verbose_name=mark_safe('k<sub>rW</sub>'))
    
    class Meta:
        model = Flow_Test_Dynamic
        attrs = {'class': "table table-striped table-sm",
                 'thead': {'class': "thead-dark"}}

class Pore_Vols_Table(tables.Table):
    pore_volume_ml = NumberColumn(orderable=False)
    vk_ml = NumberColumn(orderable=False)
    avg_co2_sat = NumberColumn(orderable=False)
    residual_brine_ml = NumberColumn(orderable=False)
    vi_ml = NumberColumn(orderable=False)
    pore_vol_co2_inj = NumberColumn(orderable=False)
    vi_per_vk = NumberColumn(orderable=False)
    corrected_vk_ml = NumberColumn(orderable=False)
    corrected_avg_co2_sat = NumberColumn(orderable=False)
    corrected_residual_brine_ml = NumberColumn(orderable=False)
    corrected_vi_per_vk = NumberColumn(orderable=False)
