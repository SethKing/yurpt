from django import forms
from .models import Rock_Sample

class RockSampleForm(forms.Form):
    """Query form to filter experiments by different rock sample properties.
    Since fields are dynamic (dependant on data in database), the form is created
    at instance creation (in __init__ instead of as class objects).  Filters are additive."""
    def __init__(self, *args, **kwargs):
        """Create query filter form with fields: rock_name, depositional_environment, rock_type,
        permeability, and porosity."""
        super().__init__(*args, **kwargs)
        Rock_Sample.objects.update()
        rock_samples = Rock_Sample.objects.all().order_by('rock_type', 'rock_name')
        rock_names = [(rs.id, rs.rock_name) for rs in rock_samples]
        initial = [c[0] for c in rock_names]
        self.fields['rock_names'] = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                                                              choices=rock_names,
                                                              initial=initial,
                                                              label='Rock Name:'
                                                              )
        dep_envs = []
        for rs in rock_samples:
            dp = (rs.depositional_environment, rs.depositional_environment)
            if dp not in dep_envs:
                dep_envs.append(dp)
        initial = [c[0] for c in dep_envs]
        self.fields['depositional_environments'] = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                                                                             choices=dep_envs,
                                                                             initial=initial,
                                                                             label="Depositional Environment:"
                                                                             )
        rock_types = []
        for rs in rock_samples:
            rt = (rs.rock_type, rs.rock_type)
            if rt not in rock_types:
                rock_types.append(rt)
        initial = [c[0] for c in rock_types]
        self.fields['rock_types'] = forms.MultipleChoiceField(widget=forms.CheckboxSelectMultiple,
                                                              choices=rock_types,
                                                              initial=initial,
                                                              label='Rock Type:'
                                                              )
        self.fields['perm_min'] = forms.FloatField(initial=0,
                                                   label='Permeability Minimum (mD):')
        self.fields['perm_max'] = forms.FloatField(initial=99000,
                                                   label='Permeability Maximum (mD):')
        self.fields['porosity_min'] = forms.FloatField(initial=0,
                                                       label='Porosity Minimum:')
        self.fields['porosity_max'] = forms.FloatField(initial=1,
                                                       label='Porosity Maximum:')
