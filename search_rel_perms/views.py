from django.http import HttpResponse
from django.shortcuts import render
from django.db.models import Q
from django.contrib.auth.decorators import login_required
from django.conf import settings
from django_tables2 import RequestConfig
from django_tables2.export.export import TableExport
from chartit import DataPool, Chart
import os
from .models import Rock_Sample, Flow_Test_Static, Flow_Test_Dynamic, Saturation_Dynamic, MSCL, Pore_Vols
from .forms import RockSampleForm
from .tables import Saturation_Dynamic_Table, MSCL_Table, Flow_Test_Table, Pore_Vols_Table

def home(request):
    """
    Home page view with link to list all experimental results and query filter form.
    On Form POST, redirects to list view.
    :param request: http request
    :return: http render
    """
    if request.method == 'POST':
        form = RockSampleForm(request.POST)
        if form.is_valid():
            cln_form = form.cleaned_data
            rsp = list(request, cln_form)
            return rsp
    else:
        form = RockSampleForm()
    context = {
        'form': form,
    }
    return render(request, 'search_rel_perms/home.html', context)

def about(request):
    """
    Static page view of info about Rel Perm Database
    :param request: http request
    :return: http render
    """
    return render(request,
        'search_rel_perms/about.html',
        {'title': 'About YURPT'})
        
def list(request, form=None):
    """
    View for list of experimental results, if form given results are filtered by form inputs.
    :param request: http request
    :param form: RockSampleForm post
    :return: http render
    """
    if form:
        q = Q()
        for rn in form['rock_names']:
            q.add(Q(id=rn), Q.OR)
        q2 = Q()
        for rt in form['rock_types']:
            q2.add(Q(rock_type=rt), Q.OR)
        q.add(q2, Q.AND)
        q2 = Q()
        for dp in form['depositional_environments']:
            q2.add(Q(depositional_environment=dp), Q.OR)
        q.add(q2, Q.AND)
        q.add(Q(absolute_perm_md__gte=form['perm_min']), Q.AND)
        q.add(Q(absolute_perm_md__lte=form['perm_max']), Q.AND)
        q.add(Q(porosity__gte=form['porosity_min']), Q.AND)
        q.add(Q(porosity__lte=form['porosity_max']), Q.AND)
        qs = Rock_Sample.objects.filter(q).order_by('rock_type', 'rock_name')
        filters = {'rock_names': ', '.join([Rock_Sample.objects.get(id=id).rock_name for id in form['rock_names']]),
                   'rock_types': ', '.join(form['rock_types']),
                   'dep_env': ', '.join(form['depositional_environments']),
                   'perm_min': form['perm_min'],
                   'perm_max': form['perm_max'],
                   'por_min': form['porosity_min'],
                   'por_max': form['porosity_max']}
    else:
        qs = Rock_Sample.objects.all().order_by('rock_type', 'rock_name')
        filters = None
    rs_list = []
    for rs in qs:
        rs_dict = {'rock_sample': rs,
                   'flow_tests': Flow_Test_Static.objects.filter(rock_sample_id=rs)}
        rs_list.append(rs_dict)
    context = {
        'query_set': rs_list,
        'no_results': len(qs) == 0,
        'filters': filters
        }
    return render(request, 'search_rel_perms/list.html', context)
    
def flow_test(request, ids=1):
    '''
    View for dynamic flow test data for a given flow test (with id=ids).  
    Creates table with django-tables2 and
    chart with django-chartit.
    '''
    # Get basic data
    ft = Flow_Test_Static.objects.get(id=ids)
    flow_data = Flow_Test_Dynamic.objects.filter(test_id=ft).order_by('id')

    # Export Data if request has _export tag.
    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        # Build Table (with non-null values)
        table = Flow_Test_Table(flow_data)
        table.exclude = ['id', 'test_id']
        RequestConfig(request).configure(table)
        exporter = TableExport(export_format, table)
        return exporter.response('{rnm}_{ftq}mlmin_flow_table.{ext}'.format(rnm=ft.rock_sample.rock_name,
                                                                            ftq=ft.q_ml_min,
                                                                            ext=export_format))
    # Build Table (with non-null values)
    fd_not_null = flow_data.exclude(s_co2_2=None)
    table = Flow_Test_Table(fd_not_null)
    table.exclude = ['id', 'test_id']

    if not(request.user.is_authenticated):
        table.exclude.extend(['time',
                              'receiving_a_p_psi',
                              'receiving_a_vol_ml',
                              'receiving_b_vol_ml',
                              'receiving_a_q_ml_min',
                              'receiving_b_p_psi',
                              'receiving_b_vol_ml',
                              'receiving_b_q_ml_min',
                              'delivery_a_p_psi',
                              'delivery_a_vol_ml',
                              'delivery_a_q_ml_min',
                              'delivery_b_p_psi',
                              'delivery_b_vol_ml',
                              'delivery_b_q_ml_min',
                              'diff_p_high_psi',
                              'setra_delivery_p_psi',
                              'setra_receive_p_psi',
                              'diff_p_low_psi',
                              ])
    RequestConfig(request).configure(table)

    # Build Chart
    if ft.flow_test_chart and os.path.exists(os.path.join(settings.MEDIA_ROOT, 'flow_test_charts', ft.flow_test_chart)):
        chart = None
        static_chart = 'flow_test_charts/' + ft.flow_test_chart
    else:
        chart_data = DataPool(
            series = [
                {'options': {
                    'source': fd_not_null},
                    'terms': ['s_co2_2', 'k_rco2', 'k_rw']
                    }])
        chart = Chart(
            datasource = chart_data,
            series_options = [{
                'options': {
                    'marker':{
                        'lineColor': ['#FF0000', '#00FF00'],
                        },
                    'type': 'scatter',
                    'stacking': False},
                'terms': {
                    's_co2_2': ['k_rco2', 'k_rw']}
                }],
            chart_options = {
                'title': {'text': 'Relative Permeability Measurements'},
                'xAxis': {
                    'title': {'text': 'S_co2_2'},
                    },
                'yAxis': {
                    'type': 'logarithmic',
                    'title': {'text': 'k_r'},
                    'min': 0.0001,
                    'max': 1.0}
                })
        static_chart = None
    # Build context dictionary for template
    context = {
        'flow_test': ft,
        'flow_table': table,
        'chart': chart,
        'static_chart': static_chart
        }
    return render(request, 'search_rel_perms/flow_test.html', context)
    
def saturation_data(request, ids=1):
    '''
    View for dynamic saturation data for a given flow test (with id=ids).  
    Creates table with django-tables2 and
    chart with django-chartit.
    '''
    # Get basic data
    flow_test = Flow_Test_Static.objects.get(id=ids)
    sat_data = Saturation_Dynamic.objects.filter(test_id=flow_test).order_by('id')
    por_vols = Pore_Vols.objects.filter(test_id=flow_test).order_by('id')

    # Build Table
    table = Saturation_Dynamic_Table(sat_data)
    for i, pv in enumerate(por_vols):
        table.columns['scan_{0:02}'.format(i+1)].column.verbose_name = (
            'Saturation at {0:0.2f} pore vols injected'.format(pv.pore_volume_ml))
        table.columns['pore_vol_{0:02}'.format(i+1)].column.verbose_name = (
            'CO2 volume at {0:0.2f} pore vols injected'.format(pv.pore_volume_ml))
    table.exclude = ['test_id', 'id']
    # Exclude unused columns for extra pore volume levels.
    for i in range(flow_test.num_pore_vol_levels + 1, 41):
        table.exclude.append('scan_{0:02}'.format(i))
        table.exclude.append('pore_vol_{0:02}'.format(i))
    RequestConfig(request).configure(table)
    # Export Data if request has _export tag.
    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('{rnm}_{ftq}mlmin_sat_table.{ext}'.format(rnm=flow_test.rock_sample.rock_name,
                                                                           ftq=flow_test.q_ml_min,
                                                                           ext=export_format))
    
    # Build Chart
    if flow_test.saturation_profile_chart and os.path.exists(os.path.join(settings.MEDIA_ROOT, 'saturation_charts', flow_test.saturation_profile_chart)):
        chart = None
        static_chart = 'saturation_charts/' + flow_test.saturation_profile_chart
    else:
        scans = []
        for i in range(1, flow_test.num_pore_vol_levels+1):
            scans.append(f'scan_{i:02}')
        chart_data = DataPool(
            series=
                [{'options':
                    {'source': sat_data},
                    'terms': ['distance'] + scans, # ['distance', 'scan_01', ...]
                        }
                ])

        chart = Chart(
            datasource = chart_data,
            series_options = [{'options': {
                'marker': {
                        'lineColor': ['#FF0000', '#00FF00'],
                        },
                'type': 'scatter',
                'stacking': False},
            'terms': {
                'distance': scans
            }}],
            chart_options =
                {'title': {
                    'text': ' '},
                'xAxis': {
                    'tickInterval': 10,
                    'title': {
                        'text': 'Distance'},
                },
                'yAxis': {'title': {'text': ' '},
                          'min': 0},
                })
        static_chart = None
    # Build context dictionary for template    
    context = {
        'flow_test': flow_test,
        'saturation_data': table,
        'chart': chart,
        'static_chart': static_chart,
        }
    return render(request, 'search_rel_perms/saturation_data.html', context)

def mscl(request, ids=1):
    """MSCL table view, basically recreate the MSCL data sheet with
    calibration parameters on top and table of measurements"""
    rs = Rock_Sample.objects.get(id=ids)
    table = MSCL_Table(MSCL.objects.filter(rock_sample=rs).order_by('id'))
    table.exclude = ['id', 'rock_sample']
    RequestConfig(request).configure(table)
    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        exporter = TableExport(export_format, table)
        return exporter.response('{rnm}_mscl_table.{ext}'.format(rnm=rs.rock_name,
                                                                 ext=export_format))
    context = {'rock_sample': rs,
               'mscl_table': table}
    return render(request, 'search_rel_perms/mscl.html', context)

def por_vol_corrections(request, ids=1):
    """This view is for the table of volume injected versus volume displaced corrections.
    The data isn't well explained and shouldn't be useful to external users so it is
    hidden from view from non-account users.  View created from input flow test id (ids)."""
    flow_test = Flow_Test_Static.objects.get(id=ids)
    por_vols = Pore_Vols.objects.filter(test_id=flow_test).order_by('id')
    table = Pore_Vols_Table(por_vols)
    table.exclude = ['id', 'test_id']
    RequestConfig(request).configure(table)
    static_chart = None
    if flow_test.correction_chart and os.path.exists(os.path.join(settings.MEDIA_ROOT, 'correction_charts', flow_test.correction_chart)):
        static_chart = 'correction_charts/' + flow_test.correction_chart
    context = {'flow_test': flow_test,
               'por_vol_table': table,
               'static_chart': static_chart}
    return render(request, 'search_rel_perms/por_vol.html', context)

def rock_sample(request, ids=1):
    """
    View for single rock sample, has rock sample properties and links to MSCL table
    as well as table of links to flow test data and saturation profiles.
    """
    rs = Rock_Sample.objects.get(id=ids)
    ft = Flow_Test_Static.objects.filter(rock_sample=rs)
    context = {
        'rock_sample': rs,
        'flow_tests': ft,
     }
    return render(request, 'search_rel_perms/rock_sample.html', context)

def about_bibtex(request):
    response = HttpResponse(content_type='text/bib')
    response['Content-Disposition'] = 'attachment; filename="yurpt.bib"'
    response.write('@Misc{Crandall2019,\n' +
                   'author =   "Dustin Crandall\n' +
                   'and Johnathan Moore\n' +
                   'and Sarah Brown\n' +
                   'and Seth King",\n' +
                   'title =    "YURPT",\n' +
                   'howpublished = {\\url{http://localhost:8000}},\n' +
                   'year = {2019}\n}')
    return response

def flow_test_defs(request):
    return render(request, 'search_rel_perms/flow_test_defs.html', {})